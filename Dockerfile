FROM debian:oldoldstable-20231218

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    libconfig-inifiles-perl libpar-packer-perl

# Cleanup
RUN apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/apt /var/lib/cache /var/lib/log

WORKDIR /mnt

CMD [ "pp", "-o", "tes3cmd.0.40-PRE-RELEASE-2.linux.x86_64", "tes3cmd" ]
